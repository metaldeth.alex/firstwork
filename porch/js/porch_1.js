// window.onload = function () {
//     const porchListElement = document.getElementById("prochListId");
//     const img = document.createElement('img');
//     img.src = '../img/cars.jpeg';
//     porchListElement.append(img);
//     const div = document.createElement('div');
//     porchListElement.innerHTML = '<div class="Test">Hello</div>';
// }


function createLiItem(floor, firstNumber, lastNumber) { 
    const liElem = document.createElement('li');
    liElem.classList.add('a2');

    const floorDivElem = document.createElement('div');
    floorDivElem.classList.add('floor_cell');
    floorDivElem.innerText = `${floor} эт.`;

    const apartmentElement = document.createElement('div');
    apartmentElement.classList.add('apartment_cell');
    apartmentElement.innerText = `${firstNumber}-${lastNumber}`;

    liElem.append(floorDivElem);
    liElem.append(apartmentElement);

    return liElem;
}

function createULFloorList(arr) {
    const porchListElement = document.getElementById("prochListId");
    porchListElement.innerText = ''; // Очистка внутриностей html элемента

    const ulElem = document.createElement('ul');

    arr.forEach(function (obj, index) {
        const liElem = createLiItem(index + 1, obj.first, obj.last);
        ulElem.append(liElem)
    })

    porchListElement.append(ulElem);
}

const floorArr = [
    { first: 1, last: 80 },
    { first: 81, last: 160 },
    { first: 161, last: 240 },
    { first: 241, last: 320 },
    { first: 321, last: 400 },
    { first: 401, last: 480 }, 
]



window.onload = function () {

    createULFloorList(floorArr);
    // setInterval(function() {
    //     const object = floorArr[floorArr.length - 1];
    //     floorArr.push({ first: object.last + 1, last: object.last + 80 });
    //     createULFloorList(floorArr);
    // }, 1000)
}

