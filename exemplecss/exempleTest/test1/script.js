$('.owl-carousel').owlCarousel({
    center: true,
    items:1,
    loop:true,
    margin:10,
    nav: true,
    navText:["<=","=>"],
    autoplay: true,
    autoplayTimeout:20,
});
