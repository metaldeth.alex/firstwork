window.onload = function(){
    const modalOpenItemArr = Array.from(document.getElementsByClassName('js-open-modal'));
    const modalBackgroudArr = Array.from(document.getElementsByClassName('modal-background'));
    const modalWindowArr = Array.from(document.getElementsByClassName('modal-window'));

    // console.log('modalOpenItemArr ' , modalOpenItemArr);
    // console.log('modalBackgroudArr ' , modalBackgroudArr);
    // console.log('modalWindowArr', modalWindowArr);

    const modalCount = document.getElementById('count');
    const modalButton = document.getElementById('button');
    const modalValue = document.getElementById('value');
    const modalClearValue = document.getElementById('clear-value');
    const modalMore = document.getElementById('more');
    const modalLess = document.getElementById('less');
    const modalEqually = document.getElementById('equally');
    let value = 0;

    // console.log('modalMore ', modalMore);
    // console.log('modalLess ', modalLess);
    // console.log('modalValue ', modalValue);
    // console.log('modalButton ', modalButton);

    const modalOpenItem = modalOpenItemArr[0];
    const modalBackgroud = modalBackgroudArr[0];
    const modalWindow = modalWindowArr[0];

    const hendlerMap = {
        1: function(){
            document.getElementById('value').innerHTML = 0;
            modalCount.style.display = 'none';
            modalButton.style.display = 'block';
            modalClearValue.style.display = 'block';
            modalMore.style.display = 'none';
            modalLess.style.display = 'none';
            modalEqually.style.display = 'none';

            console.log('bingo');

            modalButton.addEventListener('click', function(event){
                value ++;
                document.getElementById('value').innerHTML = value;
            })
        
            modalClearValue.addEventListener('click', function(){
                value = 0;
                document.getElementById('value').innerHTML = value;
            })
        },
        2: function(){
            //binarySearch
             
            let low = 0;
            let hight = 1000;
            let mid = Math.floor((low + hight) / 2);
            let count = 0;

            document.getElementById('value').innerHTML = mid;

            modalCount.style.display = 'block';
            modalButton.style.display = 'none';
            modalClearValue.style.display = 'none';
            modalMore.style.display = 'block';
            modalLess.style.display = 'block';
            modalEqually.style.display = 'block';

            modalMore.addEventListener('click', function(){
                binary(1);
            });

            modalLess.addEventListener('click', function(){
                binary(2);
            });

            modalEqually.addEventListener('click', function(){
                binary(3);
            });

            function binary(binaryValue){
                // console.log('binaryValue1 ', binaryValue);
                
                if ((low + 1) >= hight) {
                    binaryValue = 3;
                }

                if (binaryValue == 1) {
                    low = mid+1;
                }else  if (binaryValue == 2) {
                    hight = mid-1;
                } else if (binaryValue == 3) {
                    modalMore.style.display = 'none';
                    modalLess.style.display = 'none';
                    modalEqually.style.display = 'none';
                    console.log('bingo');
                    count -= 1;
                }
                mid = Math.floor((low + hight)/2);
                document.getElementById('value').innerHTML = mid;
                console.log('========================================');
                console.log('low ', low);
                console.log('hight ', hight);
                console.log('value ', value);
                console.log('========================================');
                count ++;
                binaryValue = 0;
                console.log('binaryValue ', binaryValue);
                document.getElementById('count').innerHTML = count;
            }
        }
    }


    modalOpenItemArr.forEach(element => {
        element.addEventListener('click', function(event){
            const number = event.target.dataset.number;
            hendlerMap[number]();
            // const hendler = hendlerMap[number];
            console.log('number', number);
            console.log('click');
            // console.log('hendler', hendler);
            modalBackgroud.style.display = 'block';
            // hendler();
            // console.log('event', event);


        })
    });

    modalBackgroud.addEventListener('click', function(event){
        // console.log('event', event);
        // console.log('classList', event.target.classList.contains('1'));
        // console.log('class', event.target.className);
        if  (!modalWindow.contains(event.target)) {
            modalBackgroud.style.display = 'none';
            value = 0;
        }
    })

    

}