function createOptionsByMaxFloor (maxFloor) {
    const arr = [];
    for (let i = 0 ; i < maxFloor ; i++) {
        const option = document.createElement('option');
        option.value = i + 1;
        option.innerText = `${i + 1} этаж`;
        arr.push(option);
    }
    return arr;
} 

function createOptionsByApartament(minApartament , apartamentPerFloor){
    const arr = [];
    
    for (let i = minApartament ; i < (minApartament + apartamentPerFloor + 1) ; i++) {
        const option = document.createElement('option');
        option.value = i;
        option.innerText = `${i} кв`;
        arr.push(option);
    }
    const option1 = document.createElement('option');
    option1.value = 1000;
    option1.innerText = `Лифтовый холл`;
    arr.push(option1);
    const option2 = document.createElement('option');
    option2.value = 1001;
    option2.innerText = `Лестница`;
    arr.push(option2);
    return arr;    
}

function createOptionsByPlace (placeByFloorMap , count) {
    // placeByFloorMap.identifier.length
    const arr = [];
    for (let i = 0 ; i < placeByFloorMap[count].length ; i++){
        const option = document.createElement('option');
        option.value = i + 1;
        option.innerText = placeByFloorMap[count][i];
        arr.push(option);
    }
    return arr;
}

const floorByPorchMap = {
    1: 16,
    2: 17,
    3: 10,
    4: 10,
    5: 8,
    6: 8,
}

const apartamentByFloorMap = [
    [1, 8, 9, 9, 9],
    [144, 4, 8, 8, 9],
    [290, 2, 3, 4, 4],
    [327, 3, 4, 4, 4],
    [366, 7, 9, 9, 9],
    [436, 9, 10, 10, 10],

    // {start: 1 , floor1 : 8 , floor2 : 9 , floor3 : 9,},
    // {start: 144 , floor1 : 4 , floor2 : 8 , floor3 : 8,},
    // {start: 290 , floor1 : 2 , floor2 : 3 , floor3 : 4,},
    // {start: 327 , floor1 : 3 , floor2 : 4 , floor3 : 4,},
    // {start: 366 , floor1 : 7 , floor2 : 9 , floor3 : 9,},
    // {start: 436 , floor1 : 9 , floor2 : 10 , floor3 : 10,},
];

const placeByFloorMap = [
    ['пол' , 'стены' , 'потолок' , 'двери лифта' , 'двери железные' , ],
    ['пол' , 'стены' , 'потолок' , 'дверь пвх' ,  ],
    ['пол' , 'стены' , 'потолок' , 'двери квартир' ,],
]

const globalPlace = document.getElementById('globalPlace');
const localPlace = document.getElementById('localPlace');
const exactPlace = document.getElementById('exactPlace');
const place = document.getElementById('place');
let globalPlaceNumber;



// localPlace.innerText = '';

// const options = createOptionsByMaxFloor(16);
// console.log(options);
// console.log(options[2].value);
// options.forEach(function(option){
//     localPlace.append(option);
// })

// globalPlace.addEventListener('change' , function(){
//     console.log(localPlace.value);  
// })



globalPlace.addEventListener('change' , function(){
    localPlace.innerText = '';
    globalPlaceNumber = globalPlace.value;
    console.log(globalPlaceNumber);
    const floorCount = floorByPorchMap[globalPlace.value];
    const options = createOptionsByMaxFloor(floorCount);
    options.forEach(function(option){
        localPlace.append(option);
    }) 
})
localPlace.addEventListener('change' , function() {
    // debugger;
    let count = 0;
    let apartamentPerFloor = 0;
    exactPlace.innerText = '';
    const apartamentCount = apartamentByFloorMap[globalPlaceNumber - 1] ;
    // console.log(localPlace.value);
    // console.log(globalPlaceNumber + 'globalPlaceNumber');
    // console.log(apartamentCount + 'apartament');
    // console.log(apartamentCount[0]);
    const floorCount = floorByPorchMap[globalPlace.value];
    for (let i = 1 ; i < localPlace.value ; i++) {
        if ( i == 1){
            count = apartamentCount[0] + apartamentCount[1];
            apartamentPerFloor = apartamentCount[1]
        }else if( i == 2){
            count = count + apartamentCount[2];
            apartamentPerFloor = apartamentCount[1]
        }else if( i == 3){
            count = count + apartamentCount[3];
            apartamentPerFloor = apartamentCount[1]
        }
        else{
            count = count + apartamentCount[4];
            apartamentPerFloor = apartamentCount[1]
        }
        console.log(count + '+++++' + (i + 1))
    }
    const minApartament = count;
    const options = createOptionsByApartament(minApartament , apartamentPerFloor);
    console.log(options);
    options.forEach(function(option){
        exactPlace.append(option)   
    })

})


exactPlace.addEventListener('change' , function(){
    place.innerText = '';
    let numb;
    // debugger;
    if (exactPlace.value == 1000){
        numb = 0;
    }else if(exactPlace.value ==  1001){
        numb = 1;
    } else{
        numb = 2;
    }
    console.log(`counter ${numb}`);
    placeByFloorMap.forEach(function(option){
        place.append(option);
    })
    const options = createOptionsByPlace(placeByFloorMap , numb);

    console.log(`place ${options}`);
    options.forEach(function(option){
        place.append(option)
    })
})


console.log(globalPlace);
console.log(localPlace);
console.log(exactPlace);





