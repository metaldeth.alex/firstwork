function createOptionsByMaxFloor (maxFloor) {
    const arr = [];
    for (let i = 0 ; i < maxFloor ; i++) {
        const option = document.createElement('option');
        option.value = i + 1;
        option.innerText = `${i + 1} этаж`;
        arr.push(option);
    }
    return arr;
}


const floorByPorchMap = {
    1: 16,
    2: 17,
    3: 10,
    4: 10,
    5: 8,
    6: 8,
}

const globalPlace = document.getElementById('globalPlace');
const localPlace = document.getElementById('localPlace');

// localPlace.innerText = '';

// const options = createOptionsByMaxFloor(16);
// console.log(options);
// console.log(options[2].value);
// options.forEach(function(option){
//     localPlace.append(option);
// })

// globalPlace.addEventListener('change' , function(){
//     console.log(localPlace.value);  
// })



globalPlace.addEventListener('change' , function(){
    localPlace.innerText = '';
    const floorCount = floorByPorchMap[globalPlace.value];
    const options = createOptionsByMaxFloor(floorCount);
    options.forEach(function(option){
        localPlace.append(option);
    }) 
})



